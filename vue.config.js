// vue.config.js
/*const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;*/
const webpack = require('webpack');

module.exports = {
  css: {
    extract: true
  },
  configureWebpack: {
    plugins: [
      /*new BundleAnalyzerPlugin(),*/
      new webpack.ContextReplacementPlugin(
        /moment[\/\\]locale/,
        // A regular expression matching files that should be included
        /(en-us)\.js/
      )
    ]
  }
}
