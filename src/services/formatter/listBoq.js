// import {getProjectBoqs} from '../http/boq.js'

export default class BOQListFormatter {
  constructor (dataClient) {
    this.data = {
      boqs: {},
      isLoaded: false,
      currencyType : "INR",
      countryISOCode : "IN"
    }
    this.getFormattedData(dataClient)
  }

  processData (data) {
    var fields = [
      { key: 'display_name', label: 'Name' },
      { key: 'total_price', label: 'Price' },
      { key: 'preview', label: 'Preview' }

    ]
    var items = data.data
    // console.log(items);
    for (var item of items) {
      item.preview = ''
    }

    this.data.boqs.items = items
    this.data.boqs.fields = fields

  }

  getFormattedData (dataClient) { // NOTE:
    dataClient.getProjectBoqs().then(data => {
      this.processData(data);
      var proposals = data.data;
      var proposal = null;
      if(proposals.length>0)
        proposal = proposals[0]
      if(proposal){
        if("currency" in proposal)
          this.currencyType = proposal.currency
        if("country_code" in proposal)
          this.countryISOCode = proposal.country_code
      }
      this.data.isLoaded = true;
    }).catch(error => {
      console.log(error)
    })
  }
  getData () {
    // console.log(this.data);
    return this.data
  }
}
