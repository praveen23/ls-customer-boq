import moment from 'moment'

export default class BOQFormatter {
  constructor (dataClient) {
    this.data = {
      rooms: {
        roomNames: [],
        roomItems: {}
      },
      projectSummaryData: {},
      roomBlocks: {},
      boqDetails: {},
      projectDetails: {},
      primaryDesigner: {},
      paymentSummary: {},
      termsCond: {}
    }
    this.dataClient = dataClient
  }

  processData (data, isLoaded) {
    var response = data.data
    this.data.boqDetails = response.proposal
    // var date = new Date(this.data.boqDetails.pay_by_date)
    this.data.boqDetails.pay_by_date = moment(this.data.boqDetails.pay_by_date).format('Do MMM, YYYY')
    var products = response.products
    this.data.projectDetails = response.project
    // var address = response.billing_address
    // var skuList = new Set()

    for (var item of products) {
      if (item.room_tag) {
        if (!item.room_tag.name) {
          item.room_tag.unique_name = 'others'
          item.room_tag.name = 'Others'
        }
        if (!(this.data.rooms.roomNames.includes(item.room_tag.unique_name))) {
          this.data.rooms.roomNames.push(item.room_tag.unique_name)
          this.data.rooms.roomItems[item.room_tag.unique_name] = {}
          this.data.rooms.roomItems[item.room_tag.unique_name]['itemGroups'] = {
            modular_items: {
              fields: [
                { key: 'display_name', label: 'Unit Name' },
                // { key: 'selling_price', label: 'Unit Price', class:'d-none d-sm-table-cell'},
                // { key: 'quantity', label: 'Quantity', class:'d-none d-sm-table-cell' },
                { key: 'sub_total', label: 'Price' },
                { key: 'details', label: '', class: 'col-1' }
              ],
              items: [],
              total_price: 0,
              total_quantity: 0,
              display_name: 'Modular',
              heading: 'Modular',
              isModular: true,
              unique_name: 'modular',
              isLoaded: false
            },
            civil_items: {
              fields: [
                { key: 'display_name', label: 'Item Name' },
                // { key: 'dimensions', label: 'Dimensions' },
                { key: 'quantity', label: 'Qty', class: 'd-none d-sm-block' },
                { key: 'sub_total', label: 'Price' }
              ],
              items: [],
              total_price: 0,
              total_quantity: 0,
              display_name: 'Civil Work',
              heading: 'Civil Work',
              unique_name: 'civil',
              isLoaded: false
            },
            other_items: {
              fields: [
                {key: 'display_name', label: 'Item Name', class: ''},
                {key: 'specifications', label: 'Specifications', class: 'd-none d-sm-table-cell'},
                {key: 'dimensions', label: 'Dimensions', class: 'd-none d-sm-table-cell'},
                {key: 'quantity', label: 'Qty', class: 'd-none d-sm-table-cell'},
                {key: 'sub_total', label: 'Price', class: ''}
              ],
              items: [],
              total_price: 0,
              total_quantity: 0,
              display_name: 'Others',
              heading: 'Others',
              unique_name: 'others',
              isLoaded: false
            }
          }
          this.data.rooms.roomItems[item.room_tag.unique_name]['total_quantity'] = 0
          this.data.rooms.roomItems[item.room_tag.unique_name]['total_price'] = 0
          this.data.rooms.roomItems[item.room_tag.unique_name]['display_name'] = item.room_tag.name
          this.data.rooms.roomItems[item.room_tag.unique_name]['unique_name'] = item.room_tag.unique_name
        }
        if (item.group_skus.length > 0) {
          var modular = this.data.rooms.roomItems[item.room_tag.unique_name]['itemGroups']['modular_items']

          modular['items'].push(item)
          modular['total_quantity'] += item.quantity
          modular['unique_id'] = 'modular-' + item.room_tag.id
          modular['total_price'] += Math.ceil(item.quantity * item.selling_price)
          // skuList.add(item.sku_code);
          // for (var sku of item.group_skus){
          //
          //   skuList.add(sku.sku_code);
          // }
        } else if (item.vendor_type === 'CANVAS_VENDOR') {
          var civil = this.data.rooms.roomItems[item.room_tag.unique_name]['itemGroups']['civil_items']
          civil['items'].push(item)
          civil['total_quantity'] += item.quantity
          civil['unique_id'] = 'civil-' + item.room_tag.id
          civil['total_price'] += Math.ceil(item.quantity * item.selling_price)
          // skuList.add(item.sku_code);
        } else {
          var others = this.data.rooms.roomItems[item.room_tag.unique_name]['itemGroups']['other_items']
          others['items'].push(item)
          others['total_quantity'] += item.quantity
          others['total_price'] += Math.ceil(item.quantity * item.selling_price)
          others['unique_id'] = 'others-' + item.room_tag.id
          // skuList.add(item.sku_code);
        }
        // this.rooms.roomItems[item.room_tag.unique_name]['items'].push(item);
        this.data.rooms.roomItems[item.room_tag.unique_name]['total_quantity'] += item.quantity
        this.data.rooms.roomItems[item.room_tag.unique_name]['total_price'] += Math.ceil(item.quantity * item.selling_price)
      }
    }
    // moved to different sections
    // if(skuList.size>0){
    //   var skuArray = Array.from(skuList);
    //   // Object.defineProperty(Array.prototype, 'chunk_inefficient', {
    //   //     value: function(chunkSize) {
    //   //         var array=this;
    //   //         return [].concat.apply([],
    //   //             array.map(function(elem,i) {
    //   //                 return i%chunkSize ? [] : [array.slice(i,i+chunkSize)];
    //   //             })
    //   //         );
    //   //     }
    //   // });
    //
    //   // skuArrayList = this.chunkArray(skuArray, 50);
    //   getCMSDetails(cmsDomain, skuArray).then(data =>{
    //     console.log(data)
    //     this.getRoomBlockData(data.data);
    //   }).catch(error =>{
    //     console.log(error)
    //   });
    //
    //
    // }

    this.data.projectSummaryData = this.getProjectSummaryData()
    this.data.paymentSummary = this.getPaymentSummaryData(this.data.boqDetails.net_payable)
    // var project_settings = response.project.settings
    // getUserDetails(bouncerDomain, project_settings.primary_designer_id).then(data =>{
    //   this.data.primaryDesigner = data.data;
    // }).catch(error =>{
    //   console.log(error)
    // });
    // now primary designer data is part of cluster api
    this.data.primaryDesigner = response.primary_designer
    this.data.termsCond = response.terms_cond
    // this.data.roomBlocks = this.getRoomBlockData();
    isLoaded.value = true
  }

  // chunkArray(skuArray, chunk_size){
  //     var results = [];
  //
  //     while (skuArray.length) {
  //         results.push(myArray.splice(0, chunk_size));
  //     }
  //
  //     return results;
  // }

  getPaymentSummaryData (totalPrice) {
    var paymentStructure = {
      showTh: true,
      block_heading: 'Payment Summary',
      total_price:totalPrice,
      fields: [
        {key: 'no', label: 'Sl.No', class: 'col-2 p-0 pr-4'},
        {key: 'display_name', label: 'Milestone'},
        {key: 'payable', label: 'Amount Payable', class: 'text-right text-sm-left'}

      ],
      items: [
        {
          no: 1,
          display_name: 'Advance Payment(10%)',
          payable: Math.ceil(totalPrice * 0.1)
        },
        {
          no: 2,
          display_name: 'Order Processing(40%)',
          payable: Math.ceil(totalPrice * 0.4)
        },
        {
          no: 3,
          display_name: 'Delivery & Installation(50%)',
          payable: Math.ceil(totalPrice * 0.5)
        }
      ]
    }

    return paymentStructure
  }

  setFormattedData (boqId, isLoaded) { // NOTE:
    this.dataClient.getBOQDetails(boqId).then(data => {
      this.processData(data, isLoaded)
    }).catch(error => {
      console.log(error)
    })
  }

  getData () {
    return this.data
  }

  getProjectSummaryData () {
    var summary = {
      block_heading: 'Project Summary'
    }
    var summaryFields = [
      {key: 'room', label: 'Room Name', thClass: 'brand-primary'},
      {key: 'modular', label: 'Modular Products', thClass: 'brand-primary d-none d-sm-table-cell', class: 'd-none d-sm-table-cell'},
      {key: 'others', label: 'Others', thClass: 'brand-primary d-none d-sm-table-cell', class: 'd-none d-sm-table-cell'},
      {key: 'civil', label: 'Civil Work', thClass: 'brand-primary d-none d-sm-table-cell', class: 'd-none d-sm-table-cell'},
      {key: 'price', label: 'Price', thClass: 'brand-primary', class: 'text-right text-sm-left'},
      {key: 'details', label: '', class: ' col-1 d-block d-sm-none'}
    ]
    var summaryItems = []
    var rowId = 0
    for (var room of Object.values(this.data.rooms.roomItems)) {
      var rowData = {}
      rowData['id'] = 'proj-sum' + rowId
      rowId++
      rowData['hideDesktop'] = true
      // var name = new TableElement(room.display_name, null, null);
      rowData['room_name'] = room.display_name
      // var price = new TableElement('₹'+this.numberWithCommas(room.total_price), null, null, 'text-right text-sm-left');
      rowData['total_price'] = room.total_price
      for (var itemGroup of Object.values(room.itemGroups)) {
        // var ele = new TableElement('₹'+this.numberWithCommas(itemGroup.total_price), null, null, 'text-right text-sm-left');
        rowData[itemGroup.unique_name + '_price'] = itemGroup.total_price
        this.getProjectSummaryChildren(rowData)
        rowData['static'] = true
        var childItem = {
          section_name: itemGroup.display_name
        }
        childItem['section_total'] = itemGroup.total_price
        rowData.children.items.push(childItem)
      }
      summaryItems.push(rowData)
    }
    summary['fields'] = summaryFields
    summary['items'] = summaryItems
    summary['sub_total'] = Math.ceil(this.data.boqDetails.total_price)
    summary['handling_fee'] = Math.ceil(this.data.boqDetails.handling_fee)
    summary['discount'] = Math.ceil(this.data.boqDetails.discount)
    summary['total'] = Math.ceil(Number(this.data.boqDetails.net_payable))
    return summary
  }

  getProjectSummaryChildren (item) {
    if (!('children' in item)) {
      item['children'] = {
        fields: [
          { key: 'category', label: 'Category Name' },
          { key: 'sub_total', label: 'Price' }
        ],
        items: []
      }
    }
  }

  getRoomBlockData (skuData, data) {
    var skuMap = new Map()
    for (var skuItem of skuData) {
      skuMap.set(skuItem.code, skuItem)
    }
    for (var item of data.items) {
      skuItem = skuMap.get(item.sku_code)
      if (item.group_skus.length > 0) {
        this.checkAndGetChildren(item)
        item['hideChild'] = false
        for (var groupSku of item.group_skus) {
          var newLineItem = {}
          var itemPrice = 0;
          if(groupSku.price)
            itemPrice = groupSku.price
          else if (groupSku.sub_total) {
            itemPrice = groupSku.sub_total
          }
          newLineItem['quantity'] = groupSku.quantity
          var childSkuItem = skuMap.get(groupSku.sku_code)
          if (skuMap.has(groupSku.sku_code)) {
            // newLineItem['display_name'] = [new TableElement(childSkuItem.display_name, childSkuItem, (childSkuItem.images.length>0)?childSkuItem.images[0].image_links.small:null)];
            // newLineItem['specifications'] = this.getSpecificationElements(childSkuItem.fields);
            // newLineItem['dimensions'] = [new TableElement(childSkuItem.dimensions, null, null)];
            // newLineItem['specifications'] = this.getSpecificationElements(childSkuItem.fields);
            newLineItem['metaData'] = childSkuItem
            if(!itemPrice)
              itemPrice = childSkuItem.price
          }
          newLineItem['sub_total'] = Math.ceil(groupSku.quantity*itemPrice)
          item.children.items.push(newLineItem)
        }
      }
      this.getItemTableFormat(item, skuItem)
    }
    // Vue.set(data, 'isLoaded', isLoaded)
    return true;
  }

  checkAndGetChildren (item) {
    if (!('children' in item)) {
      item.children = {
        fields: [
          {key: 'display_name', label: 'Unit Name'},
          {key: 'specifications', label: 'Specifications', class: 'd-none d-sm-table-cell'},
          {key: 'selling_price', label: 'Unit Price'},
          {key: 'quantity', label: 'Quantity', class: 'd-none d-sm-table-cell'},
          {key: 'sub_total', label: 'Price', class: 'd-none d-sm-table-cell'}
        ],
        items: []
      }
    }
  }

  getItemTableFormat (item, skuItem) {
    item.metaData = skuItem
    // item.quantity = item.quantity
    item.sub_total = Math.ceil(item.sub_total)
    item.selling_price = Math.ceil(item.selling_price)
  }

  // getSpecificationElements(fields){
  //   var elements = [];
  //   for (var field of Object.values(fields)){
  //     var metaData = field.option;
  //     var fieldName = field.display_name+" ("+metaData.display_name+") ";
  //
  //     // var element = new TableElement(fieldName, null, metaData.swatch_image_url);
  //     elements.push(element);
  //   }
  //   if(elements.length==0){
  //     var element = new TableElement("N/A", null, null);
  //     elements.push(element);
  //   }
  //   return elements;
  //
  // }
}
