import BOQFormatter from '../services/formatter/boq.js'
import RetryLoader from '../components/RetryLoader'

export default {
  props: {
    dataClient: {
      type: Object,
      required: true
    }
  },
  components: {RetryLoader},
  data () {
    return {
      isLoaded: {
        value: false,
        error: false
      }
    }
  },
  created () {
    this.refreshSkuList()
  },

  methods: {
    refreshSkuList () {
      let that = this
      that.isLoaded.value = false
      that.isLoaded.error = false
      let skuArray = []
      for (let item of this.data.items) {
        skuArray.push(item.sku_code)
        item.group_skus.forEach(function (child) {
          skuArray.push(child.sku_code)
        })
      }
      this.dataClient.getCMSDetails(skuArray).then(data => {
        let formatter = new BOQFormatter(that.dataClient)
        that.isLoaded.value = formatter.getRoomBlockData(data.data, that.data)
      }).catch(error => {
        that.isLoaded.error = true
        console.log(error)
      })
    }

  }

}
